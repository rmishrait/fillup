Fillup
=========



![Example1](_Gifs/gif.gif)


## How It Works 

1. StartGameController is initially opened nad on button click it opens LevelChooser.

2. LevelsViewController populates list of levels that can be played and on selecting levels it passes weight to StoryController for choosing words according to difficulty.

3. StoryViewController picks random page from wikipedia download it using Alamofire and parse it using Kanna then it picks 10 paragraphs and create a array of unique words present in that paragraph and sort them according to their complexity then it replaces the words picking one word from that unique words array it picks word from array according to previously passed weight then it populates the data and on clicking the blank it opens ActionPicker with list of removed words in jumbled way, and when user chooses word from picker it fills the empty blank and maintaines a dictionary for user answers.

    When user clicks on submit button it passes removed words array and user answer dictionary to ResultsViewController.

4. ResultsViewController iterates through removed words array and if position matches with dictionary element answer count is increased.

5. For Answer sheet it iterates through removed words array for actual answers and user answer dictionary for user answers.
