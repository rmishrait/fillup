//
//  Int.swift
//  Fillup
//
//  Created by Rohit on 6/13/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
extension Int
{
    public static func random (lower: Int , upper: Int) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - (lower + 1))))
    }
}
