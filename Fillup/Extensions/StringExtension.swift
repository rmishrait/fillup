//
//  StringExtension.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
extension String
{
    func stringByReplacingFirstOccurrenceOfString(
        target: String, withString replaceString: String) -> String
    {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
}
