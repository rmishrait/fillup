//
//  Utils.swift
//  Fillup
//
//  Created by Rohit on 6/13/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
class Utils{
    public static func stringToArray(string:String) -> Array<String>{
        return string.components(separatedBy: ["*", "/"," ",".","(",")",",","\n",":"])
    }
    
    public static func arrayToString(array:[String]) -> String{
        return array.joined(separator: " ")
    }
    
}
