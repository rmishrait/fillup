//
//  TouchableTextView.swift
//  Fillup
//
//  Created by Rohit on 6/13/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
class TouchableTextView : UITextView {
    
    func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        self.resignFirstResponder()
        return false
    }
    
    func shouldChangeTextInRange(range: UITextRange, replacementText text: String) -> Bool {
        self.resignFirstResponder()
        return false
    }
    
}
