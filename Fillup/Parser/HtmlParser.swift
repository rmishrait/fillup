//
//  HtmlParser.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import Kanna
class HtmlParser{
    func parseHtml(_ html:String) -> ArraySlice<String>{
        var paragraphsArray:ArraySlice<String> = []
        if let doc = Kanna.HTML(html: html, encoding: String.Encoding.utf8) {
            let paraArray = Array(doc.css("p"))
            for show in paraArray {
                let regex = "\\s?\\[[\\w\\s]*\\]"
                let showString = removeSpecialCharsFromString(text: show.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)).replacingOccurrences(of: regex, with: "", options: .regularExpression)
                if showString.characters.count > 50{
                    paragraphsArray.append(showString.appending("\n"))
                }
            }
        }
        return paragraphsArray
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_[]".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
}
