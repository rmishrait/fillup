//
//  Downloader.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import Alamofire

class Downloader{
    public var didDownloadedHTML: ((_ html: String) -> Void)?
    func downloadHTML(_ url:String){
        Alamofire.request(url).responseString { response in
            if let html = response.result.value {
                self.didDownloadedHTML?(html)
            }
        }
    }
}
