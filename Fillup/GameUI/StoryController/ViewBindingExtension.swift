//
//  ViewBindingExtension.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit
import GameKit
import ActionSheetPicker_3_0
extension StoryViewController:UITableViewDataSource, UITableViewDelegate {
    func bindTableView(){
        setupTableView()
        setupNavigationBar()
    }
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
    }
    
    func setupNavigationBar(){
        self.navigationItem.title = level
    }
    
    /**
     Shows options that can be filled
     **/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: removedWordsArray)
        ActionSheetMultipleStringPicker.show(withTitle: "Fill Blank \(indexPath.item+1)", rows: [
            shuffled
            ], initialSelection: [0], doneBlock: {
                picker, indexes, values in
                let index = indexes!.first! as! Int
                self.paragraphsArray[indexPath.item] = self.paragraphsArray[indexPath.item].replacingOccurrences(of: self.blank, with: shuffled[index] as! String)
                tableView.reloadRows(at: [indexPath], with: .none)
                self.answerDictionary[indexPath.item] = shuffled[index] as! String
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paragraphsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StoryCell
        if paragraphsArray.count >= 10 {
            cell.label.text = "\(indexPath.item+1)\(".  ")\(paragraphsArray[indexPath.item])"
        }else{
            cell.label.text = ""
        }
        return cell
    }
}
