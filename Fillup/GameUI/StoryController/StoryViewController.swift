//
//  StoryList.swift
//  Fillup
//
//  Created by Rohit on 6/13/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
class StoryViewController:UIViewController{
    
    var paragraphsArray:ArraySlice<String> = []
    var removedWordsArray:[String] = []
    let blank = "______________"
    var answerDictionary:Dictionary<Int,String> = Dictionary()
    var weight = 0
    let wiki_url = "http://en.wikipedia.org/wiki/Special:Randompage"
    var level = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var v: UIActivityIndicatorView!
    let downloader = Downloader()
    let htmlParser = HtmlParser()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindTableView()
        requestHtml()
    }
    
    /**
     Requests Html Data
     **/
    func requestHtml(){
        self.downloader.downloadHTML(wiki_url)
        self.downloader.didDownloadedHTML = {(html: String) in
            self.parseHTML(html: html)
        }
    }
    
    /**
     Parses Html
     **/
    func parseHTML(html: String) -> Void {
        paragraphsArray = htmlParser.parseHtml(html)
        if paragraphsArray.count >= 10 {
            paragraphsArray = paragraphsArray.prefix(upTo: 10)
            v.isHidden = true
            chooseWords(weight:weight)
            replaceWords()
            reloadData()
        }else{
            requestHtml()
        }
    }
    
    
    /**
     Pick words according to weights assigned from the level
     **/
    func chooseWords(weight:Int){
        for item in paragraphsArray {
            let array = Utils.stringToArray(string: item)
            var unique = Array(Set(array)).sorted() { $0.characters.count > $1.characters.count }.filter{$0 != ""}
            let isIndexValid = unique.indices.contains(weight)
            if isIndexValid {
                removedWordsArray.append(unique[weight])
            }else{
                removedWordsArray.append(unique.first!)
            }
        }
    }
    
    /**
     Replaces picked words with _____________
     **/
    func replaceWords(){
        for i in 0..<paragraphsArray.count{
            paragraphsArray[i] = paragraphsArray[i].stringByReplacingFirstOccurrenceOfString(target: removedWordsArray[i], withString: blank)
        }
    }
    
    func reloadData(){
        self.tableView.reloadData()
    }
    
    /**
     Shows Results
     **/
    @IBAction func onSubmitClick(_ sender: UIButton) {
        let destinationViewController = self.storyboard?.instantiateViewController(withIdentifier: "RESULT") as! ResultsViewController
        destinationViewController.removedWordsArray = removedWordsArray
        destinationViewController.answersDictionary = answerDictionary
        self.navigationController?.pushViewController(destinationViewController, animated: true)
    }
    
}
