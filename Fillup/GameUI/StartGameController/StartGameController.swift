//
//  StartGameController.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
class StartGameController:UIViewController{
    @IBOutlet weak var playButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    func setupButton(){
        playButton.layer.cornerRadius = 20
        playButton.layer.borderWidth = 1
        playButton.backgroundColor = UIColor.flatOrange
        playButton.layer.borderColor = UIColor.flatOrange.cgColor
    }
}
