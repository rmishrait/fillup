//
//  ResultsCalculator.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
extension ResultsViewController {
    
    func bindData(){
        total = removedWordsArray.count
        resultCount = getResult()
        scoreText.text = "You Have Scored = \(resultCount)\("/")\(total)"
        correctAnswers.text = getCorrectAnswers()
        userAnswers.text = getUserAnswers()
    }
    
    func setupNavigationBar(){
        self.navigationItem.title = "Game Over"
        self.navigationItem.setHidesBackButton(true, animated:false)
    }
    
    func getResult() -> Int{
        var c = 0
        for pos in 0..<removedWordsArray.count {
            if answersDictionary[pos] != nil{
                if removedWordsArray[pos] == answersDictionary[pos]{
                    c += 1
                }
            }
        }
        return c
    }
    
    func getCorrectAnswers() -> String {
        var answerText = ""
        for i in 0..<removedWordsArray.count{
            answerText += "\(i+1). \(removedWordsArray[i])\n"
        }
        return answerText
    }
    
    func getUserAnswers() -> String{
        var text = ""
        for i in 0..<total{
            if answersDictionary[i] == nil{
                text += "\(i+1). \(notAnsweredText) \n"
            }else{
                text += "\(i+1). \(String(describing: answersDictionary[i]!))\n"
            }
        }
        return text
    }
}
