//
//  ResultsViewController.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
import ActiveLabel
import Async
class ResultsViewController:UIViewController{
    @IBOutlet weak var scoreText: UILabel!
    
    @IBOutlet weak var correctAnswers: ActiveLabel!
    @IBOutlet weak var userAnswers: ActiveLabel!
    
    let notAnsweredText = "Not Answered "
    var resultCount = 0
    var total = 0
    var removedWordsArray:[String] = []
    var answersDictionary:Dictionary<Int,String> = Dictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.bindData()
    }
    
    @IBAction func onReplayClick(_ sender: UIButton) {
        let nav = self.navigationController
        self.navigationController?.popToRootViewController(animated: false)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LEVELS") as! LevelsViewController
        nav?.pushViewController(vc, animated: false)
    }
}
