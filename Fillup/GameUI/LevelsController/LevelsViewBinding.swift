//
//  LevelsViewBinding.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
extension LevelsViewController:UITableViewDataSource, UITableViewDelegate{
    func bindTableView(){
        setupTableView()
        setupNavigationBar()
    }
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
    }
    
    private func setupNavigationBar(){
        self.navigationItem.title = "Levels"
        self.navigationItem.setHidesBackButton(true, animated:false)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "STORY") as! StoryViewController
        vc.weight = (10-indexPath.item)
        vc.level = levels[indexPath.item]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LevelsCell
        cell.level.text = "\(levels[indexPath.item])"
        return cell
    }
}
