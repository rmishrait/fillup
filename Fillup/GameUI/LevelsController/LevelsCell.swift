//
//  LevelsCells.swift
//  Fillup
//
//  Created by Rohit on 6/14/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit
import ActiveLabel
import ChameleonFramework
class LevelsCell:UITableViewCell {
    @IBOutlet weak var level: ActiveLabel!
    override func draw(_ rect: CGRect) {
        let color = UIColor.randomFlat
        level.layer.cornerRadius = 20
        level.layer.borderWidth = 1
        level.backgroundColor = color
        level.layer.borderColor = color.cgColor
        level.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = .white
        selectedBackgroundView = bgColorView
        level.textColor = .white
    }
}
